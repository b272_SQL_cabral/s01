1. List the books Authored by Marjorie Green.
    a. The Busy Executive’s Database Guide
    b. You Can Combat Computer Stress!

2. List the books Authored by Michael O'Leary.
    a. Cooking with Computers
    b. {error: "not found"}

3. Write the author/s of "The Busy Executive’s Database Guide".
    a. Marjorie Green
    b. Abraham Bennet

4. Identify the publisher of "But Is It User Friendly?".
    a. Cheryl Carson

5. List the books published by Algodata Infosystems.
    a. The Busy Executive’s Database Guide
    b. Cooking with Computers
    c. Straight Talk with Computers
    d. But Is It User Friendly?
    e. Secrets of Silicon Valley
    f. Net Etiquette? 